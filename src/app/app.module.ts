import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { DropdownDirective } from './shared/dropdown.directive';
import { CoursesComponent } from './courses/courses.component';
import { CourseListComponent } from './courses/course-list/course-list.component';
import { CourseComponent } from './courses/course-list/course/course.component';
import { CourseEditComponent } from './courses/course-edit/course-edit.component';
import { CourseScheduleMakerComponent } from './courses/course-schedule-maker/course-schedule-maker.component';
import { CourseScheduleComponent } from './courses/course-schedule-maker/course-schedule/course-schedule.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    DropdownDirective,
    CoursesComponent,
    CourseListComponent,
    CourseComponent,
    CourseEditComponent,
    CourseScheduleMakerComponent,
    CourseScheduleComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
