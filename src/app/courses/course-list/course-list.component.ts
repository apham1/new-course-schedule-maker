import { Component, OnInit } from '@angular/core';
import { Course } from './../../shared/course.model';

import { SAMPLECOURSES } from './sample-courses';

@Component({
  selector: 'app-course-list',
  templateUrl: './course-list.component.html',
  styleUrls: ['./course-list.component.css']
})
export class CourseListComponent implements OnInit {
  courses: Course[] = SAMPLECOURSES;

  constructor() {}

  ngOnInit() {
  }

  onCourseAdded(courseToBeAdded: Course) {
    this.courses.push(courseToBeAdded);
  }

  onCoursesCleared(flag: boolean) {
    this.courses.length = 0;
  }

  // Deletes the index of the first element to match the course to be deleted
  onCourseDeleted(courseToBeDeleted: Course) {
    let index = -1;
    for (const course of this.courses) {
      if (course.equals(courseToBeDeleted)) {
        index = this.courses.indexOf(course);
        break;
      }
    }

    if (index > -1) {
      this.courses.splice(index, 1);
    }
  }
}
