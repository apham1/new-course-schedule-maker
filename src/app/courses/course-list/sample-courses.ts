import { Days } from './../../shared/days.model';
import { Time } from './../../shared/time.model';
import { TimeSlot } from './../../shared/time-slot.model';
import { Course } from './../../shared/course.model';

export const SAMPLECOURSES: Course[] = [
    new Course(
        'CS-348',
        new Array<TimeSlot>(new TimeSlot(new Time(8, 30), new Time(9, 45), new Days(false, true, false, true, false, false, false))),
        3,
        false
    ),
    new Course(
        'CS-373',
        new Array<TimeSlot>(new TimeSlot(new Time(10, 0), new Time(11, 15), new Days(false, true, false, true, false, false, false)),
                            new TimeSlot(new Time(2, 0), new Time(3, 15), new Days(false, false, true, false, true, false, false))),
        4,
        false
    ),
    new Course(
        'CS-155-OL',
        new Array<TimeSlot>(),
        3,
        true
    ),
    new Course(
        'CS-101',
        new Array<TimeSlot>(new TimeSlot(new Time(8, 30), new Time(9, 45), new Days(false, true, false, true, false, false, false))),
        3,
        false
    ),
    new Course(
        'Non-overlapping Course',
        new Array<TimeSlot>(new TimeSlot(new Time(8, 0), new Time(9, 15), new Days(false, false, true, false, true, false, false))),
        3,
        false
    )
];
