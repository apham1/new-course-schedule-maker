import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Course } from '../../../shared/course.model';

@Component({
  selector: 'app-course',
  templateUrl: './course.component.html',
  styleUrls: ['./course.component.css']
})
export class CourseComponent implements OnInit {
  @Input() course: Course;
  @Output() courseDeleted = new EventEmitter<Course>();

  constructor() { }

  ngOnInit() {
  }

  onDelete() {
    this.courseDeleted.emit(this.course);
  }
}
