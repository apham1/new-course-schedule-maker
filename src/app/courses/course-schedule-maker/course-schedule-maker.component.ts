import { Component, OnInit, Input, ViewChild, ElementRef, Output, EventEmitter } from '@angular/core';
import { Course } from '../../shared/course.model';
import { Days } from '../../shared/days.model';
import { Time } from '../../shared/time.model';
import { CourseSchedule } from './../../shared/course-schedule.model';

@Component({
  selector: 'app-course-schedule-maker',
  templateUrl: './course-schedule-maker.component.html',
  styleUrls: ['./course-schedule-maker.component.css']
})
export class CourseScheduleMakerComponent implements OnInit {
  @Input() courses: Course[];
  @ViewChild('minCreditsInput', { static: false }) minCreditsInputRef: ElementRef;
  @ViewChild('maxCreditsInput', { static: false }) maxCreditsInputRef: ElementRef;
  minCredits: number;
  maxCredits: number;
  courseSchedules: CourseSchedule[] = [];
  showTimeSlots = true;

  constructor() { }

  ngOnInit() {
  }

  createNewSchedules() {
    this.courseSchedules = [];

    this.minCredits = this.minCreditsInputRef.nativeElement.value;
    if (isNaN(this.minCredits)) {
      this.minCredits = 0;
    }

    this.maxCredits = this.maxCreditsInputRef.nativeElement.value;
    if (isNaN(this.maxCredits)) {
      this.maxCredits = 19;
    }

    let i = 0;
    let fromIterator = 0;
    outerloop:
    while (i < this.courses.length) {
      const newSchedule: CourseSchedule = new CourseSchedule();
      newSchedule.courses.push(this.courses[i]);
      newSchedule.credits += this.courses[i].credits;

      if (this.addScheduleToList(newSchedule)) {
        continue;
      }

      for (let j = fromIterator; j < this.courses.length; j++) {
        if (!this.courses[j].overlapsWithSchedule(newSchedule.courses) && !this.courses[i].equals(this.courses[j])) {
          newSchedule.courses.push(this.courses[j]);
          newSchedule.credits += this.courses[j].credits;

          if (this.addScheduleToList(newSchedule)) {
            continue outerloop;
          }
        }
      }
      if (fromIterator !== this.courses.length - 1) {
        fromIterator++;
      } else {
        fromIterator = 0;
        i++;
      }
    }

    this.sortScheduleList(this.courseSchedules);
  }

  resetSchedule(schedule: Course[]) {
    schedule = [];
  }

  addScheduleToList(scheduleToAdd: CourseSchedule): boolean {
    let isWithinCreditRange = true;
    if (scheduleToAdd.credits > this.maxCredits || scheduleToAdd.credits < this.minCredits) {
      isWithinCreditRange = false;
    }

    if (isWithinCreditRange) {
      if (this.courseSchedules.length !== 0) {
        let isUnique = true;
        for (const courseSchedule of this.courseSchedules) {
          if (this.schedulesAreEqual(scheduleToAdd.courses, courseSchedule.courses)) {
            isUnique = false;
          }
        }
        if (isUnique) {
          this.courseSchedules.push(scheduleToAdd);
          return true;
        }
      } else {
        this.courseSchedules.push(scheduleToAdd);
        return true;
      }
    }
    return false;
  }

  sortScheduleList(scheduleList: CourseSchedule[]) {
    for (let i = 0; i < scheduleList.length; i++) {
      for (let j = i; j < scheduleList.length; j++) {
        if (scheduleList[i].courses.length < scheduleList[j].courses.length) {
          const temp = scheduleList[i];
          scheduleList[i] = scheduleList[j];
          scheduleList[j] = temp;
        }
      }
    }
  }

  schedulesAreEqual(schedule1: Course[], schedule2: Course[]): boolean {
    if (schedule1.length !== schedule2.length) {
      return false;
    }

    let equalElementCount = 0;
    for (let i = 0; i < schedule1.length; i++) {
      for (let j = 0; j < schedule2.length; j++) {
        if (schedule1[i].equals(schedule2[j])) {
          equalElementCount++;
        }
      }
    }

    if (equalElementCount === schedule1.length && equalElementCount !== 0) {
      return true;
    }

    return false;
  }
}
