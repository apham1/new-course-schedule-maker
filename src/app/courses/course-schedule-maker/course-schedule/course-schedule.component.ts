import { Component, OnInit, Input } from '@angular/core';
import { Course } from './../../../shared/course.model';
import { Time } from './../../../shared/time.model';
import { Days } from './../../../shared/days.model';
import { CourseSchedule } from './../../../shared/course-schedule.model';

@Component({
  selector: 'app-course-schedule',
  templateUrl: './course-schedule.component.html',
  styleUrls: ['./course-schedule.component.css']
})
export class CourseScheduleComponent implements OnInit {
  @Input() courseSchedule: CourseSchedule;
  @Input() showTimeSlots: boolean;
  @Input() index: number;

  constructor() { }

  ngOnInit() {
  }
}
