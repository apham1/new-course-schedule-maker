import { Component, ViewChild, Output, EventEmitter, OnInit, ElementRef } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Course } from '../../shared/course.model';
import { Days } from './../../shared/days.model';
import { Time } from '../../shared/time.model';
import { TimeSlot } from './../../shared/time-slot.model';

@Component({
  selector: 'app-course-edit',
  templateUrl: './course-edit.component.html',
  styleUrls: ['./course-edit.component.css']
})
export class CourseEditComponent implements OnInit {
  @Output() courseAdded = new EventEmitter<Course>();
  @Output() coursesCleared = new EventEmitter<Boolean>();

  courseName: string;
  credits: number;
  startTimes: string[] = [];
  endTimes: string[] = [];
  timeSlots: TimeSlot[] = [new TimeSlot(new Time(0, 0), new Time(0, 0), new Days())];
  isOnline = false;

  validInput: boolean;
  errorMessages: string[];
  lastAddedCourse: Course;
  indexes: Number[] = [0];

  minTimeSlots = 1;
  maxTimeSlots = 10;
  deleteTimeSlotIsDisabled = true;
  addTimeSlotIsDisabled = false;

  constructor() { }

  ngOnInit() {

  }

  onAddItem() {
    const courseName = this.courseName;
    const courseTimeSlots = new Array<TimeSlot>();
    const courseCredits = Number(this.credits);

    if (!this.isOnline) {
      for (let i = 0; i < this.indexes.length; i++) {
        let courseStartTime: string[];
        if (this.startTimes[i] !== undefined) {
          courseStartTime = this.startTimes[i].split(':');
        } else {
          break;
        }
        let courseEndTime: string[];
        if (this.endTimes[i] !== undefined) {
          courseEndTime = this.endTimes[i].split(':');
        } else {
          break;
        }
        const courseDays = new Days(this.timeSlots[i].days.sunday,
                                    this.timeSlots[i].days.monday,
                                    this.timeSlots[i].days.tuesday,
                                    this.timeSlots[i].days.wednesday,
                                    this.timeSlots[i].days.thursday,
                                    this.timeSlots[i].days.friday,
                                    this.timeSlots[i].days.saturday);
        courseTimeSlots.push(new TimeSlot(new Time(Number(courseStartTime[0]), Number(courseStartTime[1])),
                                    new Time(Number(courseEndTime[0]), Number(courseEndTime[1])),
                                    courseDays));
      }

      this.validateInput(courseName, courseTimeSlots, courseCredits);
      if (this.validInput) {
        this.addCourse(courseName, courseTimeSlots, courseCredits);
      }
    } else {
      this.addCourse(courseName, courseTimeSlots, courseCredits);
    }
  }

  addCourse(name: string, timeSlots: TimeSlot[], credits: number) {
    const newCourse = new Course(name,
                                 timeSlots,
                                 credits,
                                 this.isOnline);
    this.courseAdded.emit(newCourse);
    this.lastAddedCourse = newCourse;
    this.resetForm();
  }

  validateInput(name: string, timeSlots: TimeSlot[], credits: number) {
    this.errorMessages = [];
    let isValid = true;

    if (name !== undefined) {
      if (name.length <= 0 || name === null) {
        isValid = false;
        this.errorMessages.push('Invalid name.');
      }
    } else {
      isValid = false;
      this.errorMessages.push('Name is undefined.');
    }

    if (credits < 0 || credits === null || isNaN(credits)) {
      isValid = false;
      this.errorMessages.push('Invalid credits.');
    }

    if (timeSlots.length > 0) {
      let timeSlotsAreValid = true;
      for (let i = 0; i < timeSlots.length; i++) {
        if (isNaN(timeSlots[i].startTime.hours) || isNaN(timeSlots[i].startTime.minutes)) {
          timeSlotsAreValid = false;
          this.errorMessages.push('Invalid start time. (Time slot: ' + i + ')');
        }
        if (isNaN(timeSlots[i].endTime.hours) || isNaN(timeSlots[i].endTime.minutes)) {
          timeSlotsAreValid = false;
          this.errorMessages.push('Invalid end time. (Time slot: ' + i + ')');
        }
        if (timeSlots[i].days.isAllFalse()) {
          timeSlotsAreValid = false;
          this.errorMessages.push('Invalid days. (Time slot: ' + i + ')');
        }
      }
      if (!timeSlotsAreValid) {
        isValid = false;
      }
    } else {
      isValid = false;
      this.errorMessages.push('Invalid times.');
    }

    this.validInput = isValid;
  }

  onClear() {
    this.coursesCleared.emit(true);
  }

  resetForm() {
    this.courseName = '';
    this.credits = null;
    for (let i = 0; i < this.indexes.length; i++) {
      this.startTimes[i] = '';
      this.endTimes[i] = '';
      this.timeSlots[i].days = new Days();
    }
    this.indexes.length = 1;
  }

  onAddTimeSlot() {
    this.deleteTimeSlotIsDisabled = false;
    if (this.indexes.length < this.maxTimeSlots) {
      this.indexes.push(this.indexes.length);
      this.timeSlots.push(new TimeSlot(new Time(0, 0), new Time(0, 0), new Days()));

      if (this.indexes.length === this.maxTimeSlots) {
        this.addTimeSlotIsDisabled = true;
      }
    }
  }

  onDeleteTimeSlot() {
    this.addTimeSlotIsDisabled = false;
    if (this.indexes.length > this.minTimeSlots) {
      this.indexes.splice(this.indexes.length - 1, 1);
      this.timeSlots.splice(this.timeSlots.length - 1, 1);

      if (this.indexes.length === this.minTimeSlots) {
        this.deleteTimeSlotIsDisabled = true;
      }
    }
  }
}
