import { TimeSlot } from './time-slot.model';

export class Course {
    public name: string;
    public timeSlots: TimeSlot[];
    public credits: number;
    public isOnline: boolean;

    constructor(name: string, timeSlots: TimeSlot[], credits: number, isOnline: boolean) {
        this.name = name;
        this.timeSlots = timeSlots;
        this.credits = credits;
        this.isOnline = isOnline;
    }

    equals(course: Course): boolean {
        if (this.name !== course.name) {
            return false;
        }
        if (this.credits !== course.credits) {
            return false;
        }

        if ((this.isOnline && !course.isOnline) || (this.isOnline && !course.isOnline)) {
            return false;
        } else if (this.isOnline && course.isOnline) {
            return true;
        } else {
            if (this.timeSlots.length !== course.timeSlots.length) {
                return false;
            } else {
                let equalCount = 0;
                for (const timeSlot1 of this.timeSlots) {
                    for (const timeSlot2 of course.timeSlots) {
                        if (timeSlot1.equals(timeSlot2)) {
                            equalCount++;
                            break;
                        }
                    }
                }
                if (equalCount !== this.timeSlots.length) {
                    return false;
                }
            }
        }

        return true;
    }

    overlapsWithSchedule(schedule: Course[]): boolean {
        for (let i = 0; i < schedule.length; i++) {
            if (this.overlapsWith(schedule[i])) {
                return true;
            }
        }
        return false;
    }

    overlapsWith(course: Course): boolean {
        if (this.checkIfAnyDaysOverlap(course)) {
            let equalCount = 0;
            for (const timeSlot1 of this.timeSlots) {
                for (const timeSlot2 of course.timeSlots) {
                    if ((timeSlot1.endTime.convertToMinutes() >= timeSlot2.startTime.convertToMinutes() &&
                         timeSlot1.endTime.convertToMinutes() <= timeSlot2.endTime.convertToMinutes())
                         ||
                         timeSlot1.startTime.convertToMinutes() >= timeSlot2.startTime.convertToMinutes() &&
                         timeSlot1.startTime.convertToMinutes() <= timeSlot2.endTime.convertToMinutes()) {
                        equalCount++;
                        break;
                    }
                }
            }
            if (equalCount === this.timeSlots.length) {
                return true;
            }
        }
        return false;
    }

    checkIfAnyDaysOverlap(course: Course): boolean {
        if (this.isOnline && course.isOnline) {
            return false;
        } else if (this.timeSlots.length !== course.timeSlots.length) {
            return false;
        } else {
            let equalCount = 0;
            for (const timeSlot1 of this.timeSlots) {
                for (const timeSlot2 of course.timeSlots) {
                    if (timeSlot1.days.overlapsWith(timeSlot2.days)) {
                        equalCount++;
                        break;
                    }
                }
            }
            if (equalCount === this.timeSlots.length) {
                return true;
            }
        }
        return false;
    }
}
