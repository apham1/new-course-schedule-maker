import { Course } from './course.model';

export class CourseSchedule {
    public courses: Course[];
    public credits: number;
    constructor(courses: Course[] = [], credits: number = 0) {
        this.courses = courses;
        this.credits = credits;
    }
}
