export class Time {
    constructor(public hours: number, public minutes: number) {
        if (this.hours < 0 || this.hours > 24) {
            this.hours = 0;
        } else if (this.hours > 23) {
            this.hours = 23;
        }

        if (this.minutes < 0) {
            this.minutes = 0;
        } else if (this.minutes > 59) {
            this.minutes = 59;
        }
    }

    toString(): string {
        let output = '';
        let midnight = false;

        if (this.hours === 0) {
            midnight = true;
            output = '12';
        } else if (this.hours <= 12) {
            output = this.hours.toString();
        } else {
            output = (this.hours - 12).toString();
        }

        output += ':';

        if (this.minutes < 10) {
            output += '0' + this.minutes.toString();
        } else {
            output += this.minutes.toString();
        }

        if (this.hours < 12 || midnight) {
            output += 'AM';
        } else {
            output += 'PM';
        }

        return output;
    }

    equals(time: Time): boolean {
        if (this.hours === time.hours &&
            this.minutes === time.minutes) {
                return true;
            }
        return false;
    }

    convertToMinutes(): number {
        return this.hours * 60 + this.minutes;
    }
}
