export class Days {
    constructor(public sunday: boolean = false,
        public monday: boolean = false,
        public tuesday: boolean = false,
        public wednesday: boolean = false,
        public thursday: boolean = false,
        public friday: boolean = false,
        public saturday: boolean = false) {}

    toString(): string {
        let output = '';

        if (this.sunday) {
            output += 'S ';
        }
        if (this.monday) {
            output += 'M ';
        }
        if (this.tuesday) {
            output += 'T ';
        }
        if (this.wednesday) {
            output += 'W ';
        }
        if (this.thursday) {
            output += 'Th ';
        }
        if (this.friday) {
            output += 'F ';
        }
        if (this.saturday) {
            output += 'Sa ';
        }

        return output.substr(0, output.length - 1);
    }

    reset() {
        this.sunday = false;
        this.monday = false;
        this.tuesday = false;
        this.wednesday = false;
        this.thursday = false;
        this.friday = false;
        this.saturday = false;
    }

    overlapsWith(days: Days): boolean {
        let overlap = false;
        if (this.monday && days.monday) {
            overlap = true;
        }
        if (this.tuesday && days.tuesday) {
            overlap = true;
        }
        if (this.wednesday && days.wednesday) {
            overlap = true;
        }
        if (this.thursday && days.thursday) {
            overlap = true;
        }
        if (this.friday && days.friday) {
            overlap = true;
        }
        if (this.saturday && days.saturday) {
            overlap = true;
        }
        if (this.saturday && days.saturday) {
            overlap = true;
        }
        return overlap;
    }

    equals(days: Days): boolean {
        if ((this.monday === days.monday) &&
            (this.tuesday === days.tuesday) &&
            (this.wednesday === days.wednesday) &&
            (this.thursday === days.thursday) &&
            (this.friday === days.friday) &&
            (this.saturday === days.saturday) &&
            (this.saturday === days.saturday)) {
                return true;
        }

        return false;
    }

    isAllFalse(): boolean {
        if (this.sunday === false &&
            this.monday === false &&
            this.tuesday === false &&
            this.wednesday === false &&
            this.thursday === false &&
            this.friday === false &&
            this.saturday === false) {
            return true;
         }
         return false;
    }
}
