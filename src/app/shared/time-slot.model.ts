import { Days } from './days.model';
import { Time } from './time.model';

export class TimeSlot {
    constructor(public startTime: Time, public endTime: Time, public days: Days) {}

    equals(timeSlot: TimeSlot): boolean {
        if (this.startTime.equals(timeSlot.startTime) &&
            this.endTime.equals(timeSlot.endTime) &&
            this.days.equals(timeSlot.days)) {
            return true;
        }
        return false;
    }
}
